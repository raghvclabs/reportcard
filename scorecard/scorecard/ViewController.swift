//
//  ViewController.swift
//  scorecard
//
//  Created by Click Labs on 1/13/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var resultView: UIView!
    
    @IBOutlet weak var classLabel: UILabel!
    
    @IBOutlet weak var ageLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var computerLabel: UILabel!
    
    @IBOutlet weak var hindiLabels: UILabel!
    
    @IBOutlet weak var scienceLabel: UILabel!
    
    @IBOutlet weak var mathsLabel: UILabel!
    
    @IBOutlet weak var congrats: UIImageView!
    
    @IBOutlet weak var englishLabel: UILabel!
    
    @IBOutlet weak var percentageLabel: UILabel!
    
    
    @IBAction func calcButton(sender: AnyObject) {
        self.view.endEditing(true)
        
        if( englishMarks.text == "" || mathsMarks.text == "" || scienceMarks.text == "" || hindiMarks.text == "" || computersMarks.text == "" )
    
        {
            errorLabel.text = "incomplete info"
        
            resultView.hidden = true
            if englishMarks.text == "" {
                englishMarks .becomeFirstResponder()
            }
            else if mathsMarks.text == "" {
                mathsMarks .becomeFirstResponder()
            }
            else if scienceMarks.text == "" {
                scienceMarks .becomeFirstResponder()
            }
            else if hindiMarks.text == "" {
                hindiMarks .becomeFirstResponder()
            }
            else {
                computersMarks .becomeFirstResponder()
            }
        }
        else if ( englishMarks.text.toInt() > 100 || mathsMarks.text.toInt() > 100  || scienceMarks.text.toInt() > 100 || hindiMarks.text.toInt() > 100 || computersMarks.text.toInt() > 100 )
            
            {
                errorLabel.text = "wrong info enter between 0 to 100"
                
                resultView.hidden = true
                if englishMarks.text.toInt() > 100 {
                englishMarks .becomeFirstResponder()
                }
                else if mathsMarks.text.toInt() > 100 {
                    mathsMarks .becomeFirstResponder()
                }
                else if scienceMarks.text.toInt() > 100 {
                    scienceMarks .becomeFirstResponder()
                }
                else if hindiMarks.text.toInt() > 100 {
                    hindiMarks .becomeFirstResponder()
                }
                else {
                computersMarks .becomeFirstResponder()
                }
                
        }

        else
        {
            errorLabel.text = ""
        resultView.hidden = false
        nameLabel.text = name.text!
        classLabel.text = classField.text!
        ageLabel.text = age.text!
        computerLabel.text = computersMarks.text!
        hindiLabels.text = hindiMarks.text!
        scienceLabel.text = scienceMarks.text!
        mathsLabel.text = mathsMarks.text!
        englishLabel.text = englishMarks.text!
        
        var result = 0
        
            result = englishMarks.text.toInt()! + mathsMarks.text.toInt()! + hindiMarks.text.toInt()! + scienceMarks.text.toInt()! + computersMarks.text.toInt()!
        totalMarks.text = "\(result)"
            percentageLabel.text = " \(result/5) % "
        if (result  >= 450) {
          grade.text = "A"
            var image = UIImage(named:"image1.jpg")
            imagePlane.image = image
        }
        else if (result >= 400){
            grade.text = "B"
            var image = UIImage(named:"image2.jpg")
            imagePlane.image = image

        }
        else {
            grade.text = "C"
            var image = UIImage(named:"image4.jpeg")
            imagePlane.image = image

        }
        
        
        }
        
    }
    @IBOutlet weak var imagePlane: UIImageView!
    
    @IBOutlet weak var grade: UILabel!
    
    @IBOutlet weak var totalMarks: UILabel!
    
    @IBOutlet weak var computersMarks: UITextField!
    
    @IBOutlet weak var scienceMarks: UITextField!
    
    @IBOutlet weak var mathsMarks: UITextField!
    
    @IBOutlet weak var hindiMarks: UITextField!
    
    @IBOutlet weak var englishMarks: UITextField!
    
    @IBOutlet weak var classField: UITextField!
    
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var age: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

